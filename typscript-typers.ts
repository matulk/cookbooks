// can be used to "unwrap" a Promise type:
type ThenArg<T> =
  T extends Promise<infer U> ? U :
  T extends (...args: any[]) => Promise<infer U> ? U :
  T
// example:
async function promiseOne() {
  return 1
}

type AsyncReturnType = ThenArg<typeof promiseOne>


// React PropTypes it TS interface -----------------------------------------

const proptypes = {
  shape: PropTypes.shape({num: PropTypes.number}).isRequired,
  objectOf: PropTypes.objectOf(PropTypes.number),
  exact: PropTypes.exact({value: PropTypes.number}).isRequired,
  src: PropTypes.string,
  num: PropTypes.number.isRequired,
  nums: PropTypes.arrayOf(PropTypes.number),
  // stuff: PropTypes.object({t: PropTypes.string, s: PropTypes.number})
}

type TReactPropTypes<T> = {[key in keyof T]: TReactPropType<T[key]>}

type TReactPropType<P> =
  P extends string|number|any[] ? P :
  P extends PropTypes.Requireable<infer T> ? TReactPropTypetype<T>|null :
  P extends PropTypes.Validator<infer T> ? TReactPropTypetype<T> : any
  
type TReactPropTypetype<T> =
  T extends string|number|any[] ? T :
  T extends {[k:string]:number|string|any[]|null} ? T :
  T extends PropTypes.InferProps<infer R> ? TReactPropTypes<R>
  : any;

// type Pr = TReactPropTypes<typeof proptypes>
type Pr = TReactPropTypes<typeof proptypes>
let pr:Pr = {} as any;
pr.objectOf!.sdf;
pr.shape.num