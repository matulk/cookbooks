# WebGL Cookbook

## crunch: create DXT5 compressed textures with

```bash
./crunch -file LeadenhallMarket.jpg -fileformat ktx -yflip -rescale 1024 1024 -DXT5 -out LeadenhallMarket-dxt.ktx -dxtQuality uber -quality 255 -forceprimaryencoding
```

##babylonjs: create dxt5 texture:
```js
engine.setTextureFormatToUse(['-dxt.ktx'])
const texture = new BABYLON.Texture(`file.png`, scene)
 // or using nodejs fs
const data = fs.readFileSync(`file-dxt.ktx`)
const arraybuffer = data.buffer.slice(data.byteOffset, data.byteOffset + data.byteLength)
const textureUsingNodejs = new BABYLON.Texture(`data:file-dxt.ktx`, scene, false, true, b.Texture.BILINEAR_SAMPLINGMODE, undefined, undefined, arraybuffer)

```

