# Adobe Air Cookbook

## Creating a Self Signed Certificate
/Users/mak/Library/Developer/Adobe/AIRSDK_Compiler_24/bin/adt -certificate -cn SelfSign 2048-RSA newcert.p12 <password>

## Creating a Bundle
adt -package -keystore newcert.p12 -storetype pkcs12 -target bundle myApp.app myApp-app.xml myApp.swf icons resources

## Creating a Bundle (with password)
adt -package -keystore newcert.p12 -storetype pkcs12 -storepass <password> -target bundle myApp.app myApp-app.xml myApp.swf icons resources