# rx-js cookbook

## Ajax call

```javascript
import { ajax } from 'rxjs/observable/dom/ajax';
import { AjaxRequest } from 'rxjs/Rx';
import { AjaxResponse } from 'rxjs/Rx';

ajax({ url, method: 'GET', responseType: 'json' })
.subscribe( res => console.log(res.response)) 
```

## Retry (if errored | wait for 5 seconds)
```javascript
  Observable.defer(() => SOME_OBSERVABLE().retryWhen( errors => errors.delay(5000)).subscribe()
```