# Git Cookbook

## forgot something?
1. stage the extra changes

    ```bash
    git add .
    ```

1. commit again

    ```bash
    git commit --amend
    ```

see also:
<https://nathanhoad.net/git-amend-your-last-commit>


## git fetch doesn't fetch all branches
1. list actual remote branches

    ```bash
    git ls-remote
    ```

1. check git config

    ```bash
    git config --get remote.origin.fetch
    ```

1. update config

    ```bash
    git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
    ```

1. fetch and check and checkout

    ```bash
    git fetch -a
    git branch -r
    git checkout <branchname>
    ```

see also:
<http://stackoverflow.com/questions/11623862/git-fetch-doesnt-fetch-all-branches/25941875#25941875>

## Ignore files that have already been committed

To untrack a single file that has already been added/initialized to your repository, i.e., stop tracking the file but not delete it from your system use: `git rm --cached filename`.

To untrack every file that is now in your .gitignore first commit any outstanding code changes, and then, run this command:

```bash
git rm -r --cached .
```

This removes any changed files from the index(staging area), then just run:

```bash
git add .
```

Commit it:
```bash
git commit -m ".gitignore is now working"
```
see:
<http://stackoverflow.com/questions/1139762/ignore-files-that-have-already-been-committed-to-a-git-repository>