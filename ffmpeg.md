# FFMPEG Cookbook

## Pipe image files (png/jpg)
```bash
ffmpeg -y -f image2pipe -r 60 -i - phiena.mp4
```
## Pipe raw Bitmap bytes
```bash
ffmpeg -y -f rawvideo -pix_fmt argb -s 4320x1920 -r 60 -i - -c:v libx264 -pix_fmt yuva420p
```
## Convert image sequence to video
```bash
ffmpeg -f image2 -i rendered/picture%d.jpg -r 60 test.mp4
```
## Muxing
```bash
ffmpeg -i video.mp4 -i sound.wav -map 0:0 -map 1:0 -c:v copy -c:a aac -b:a 192k out.mp4
```

## webm with transparency (for Unity3d)
```bash
ffmpeg.exe -c:v libvpx -i .video.mp4 -c:v libvpx -auto-alt-ref 0 -pix_fmt yuva420p -metadata:s:v:0 alpha_mode="1" -threads 4 -slices 6 -crf 6 -qmin 2 -qmax 50 -b:v 20M -acodec copy -y video.webm
```
## webm to transparent png sequence with transparency
```
ffmpeg -vcodec libvpx -i video.webm frames%4d.png
```